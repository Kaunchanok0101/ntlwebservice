package com.ntl.api.NTLWebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NtlWebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NtlWebserviceApplication.class, args);
	}

}
