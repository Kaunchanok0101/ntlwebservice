package com.ntl.api;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.ntl.api.constant.GlobalConstant;

@SpringBootApplication
public class NtlWebserviceApplication extends SpringBootServletInitializer implements CommandLineRunner{
	
	@Value(GlobalConstant.SPRING_PROFILES_ACTIVE)
    private String activeProfile;

	public static void main(String[] args) {
		SpringApplication.run(NtlWebserviceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("|------------ Details ------------");
        System.out.println();
        System.out.println("|--- Environment : " + activeProfile);
        System.out.println();
        System.out.println("|--- Default Timezone : " + TimeZone.getDefault().getID() + " at Date : " + new Date());
        System.out.println();
        System.out.println("|--- Application Charset : " + Charset.defaultCharset().displayName());
        System.out.println();
        System.out.println("|---------------------------------");
	}

}
