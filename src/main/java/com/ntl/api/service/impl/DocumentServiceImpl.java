package com.ntl.api.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.googlecode.jthaipdf.jasperreports.engine.ThaiExporterManager;
import com.ntl.api.constant.BusinessConstant;
import com.ntl.api.service.DocumentService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 * @author Kaunchanok.O
 */
@Service
public class DocumentServiceImpl implements DocumentService{
	
	@Autowired
    private DataSource dataSource;

	@Override
	public byte[] generatePdf(JasperReport jasperReport, Map<String, Object> params) throws Exception {
		byte[] out = null;

        try (Connection conn = dataSource.getConnection()) {

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            try {

                // Note :  Export Part (Thai Upper Tone Characters Correcting is Supported)
                ByteArrayOutputStream reportByteArrayOutputStream = new ByteArrayOutputStream();
                ThaiExporterManager.exportReportToPdfStream(jasperPrint, reportByteArrayOutputStream);
                out = reportByteArrayOutputStream.toByteArray();
                System.out.println("");
            } catch (Exception e){

                // Note : If Export as Thai Document Failed... Then Export with Original 'JasperFillManager.fillReport(..)'
                // Note : Old Export Part (Thai Upper Tone Characters Correcting is Not Support)
//                logger.printStackTraceToErrorLog(getRefId4Log(TAG, "generatePdf"), e.getClass().getSimpleName(), e);
                out = JasperExportManager.exportReportToPdf(jasperPrint);

            }

        } catch (JRException e) {
            e.printStackTrace();
//            logger.printStackTraceToErrorLog(getRefId4Log(TAG, "generatePdf"), e.getClass().getName(), e);
//            throw new ServiceException(getMessage(ReferenceConstant.Message.MESSAGE_ERROR_CREATE_PDF, new Object[]{"ข้อผิดพลาดในขั้นตอนสร้างเอกสารโดย Jasper (JRException)"}));

        } catch (Exception e) {
            e.printStackTrace();
//            logger.printStackTraceToErrorLog(getRefId4Log(TAG, "generatePdf"), e.getClass().getName(), e);
//            throw new ServiceException(getMessage(ReferenceConstant.Message.MESSAGE_ERROR_CREATE_PDF, new Object[]{"ข้อผิดพลาดในขั้นตอนสร้างเอกสารโดย Jasper (Exception)"}));

        }

        return out;
	}

	@Override
	public byte[] generateInvoiceDeliveryOrderReport() throws Exception {
		InputStream fis = new ClassPathResource(BusinessConstant.DEFAULT_REPORT_PATH + BusinessConstant.BLANK_A4.concat(".jasper")).getInputStream();
        Map<String, Object> params = new HashMap<>();
//        params.put("documentId", BusinessConstant.INVOICE_DELIVERY_ORDER_FILE);
//        params.put("customer_name", "Test");
//        params.put("transId", generateDocumentFactor.getTransId());

        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(fis);
        return generatePdf(jasperReport, params);
	}
	
}
