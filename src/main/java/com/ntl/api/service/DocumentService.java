package com.ntl.api.service;

import java.util.Map;

import net.sf.jasperreports.engine.JasperReport;

/**
 * @author Kaunchanok.O
 */
public interface DocumentService {
	
	byte[] generatePdf(JasperReport jasperReport, Map<String, Object> params) throws Exception;
	byte[] generateInvoiceDeliveryOrderReport() throws Exception;

}
