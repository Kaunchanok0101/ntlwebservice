package com.ntl.api.controller.rest;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ntl.api.service.DocumentService;

/**
 * @author Kaunchanok.O
 */
@RestController
public class DocumentController {
	
	@Autowired
    DocumentService documentService;
	
	@PostMapping("/printPdf")
	public String printPdf(){
		System.out.println("/printPdf");
		try {
			byte[] bos = documentService.generateInvoiceDeliveryOrderReport();
			File generatedDocument = new File("D:\\workspace_learn\\default.pdf");
			
			if (bos != null) { // Case : Report Generated Object is Not Null...
				System.out.println("Generate Document File");
				FileUtils.writeByteArrayToFile(generatedDocument, bos);
            } else { // Case : Yes! Report Generated Object is Null...
            	System.out.println("Document Cannot be Generated! Output Document is NULL");
            }
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "generate report success.";
	}

}
