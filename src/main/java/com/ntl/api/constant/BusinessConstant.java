package com.ntl.api.constant;

/**
 * @author Kaunchanok.O
 */
public class BusinessConstant {
	
	//Report
	public static final String DEFAULT_REPORT_PATH = "static/document/report/jasper/";
	
	public static final String INVOICE_DELIVERY_ORDER = "INV_DEORD";
	public static final String INVOICE_DELIVERY_ORDER_FILE = "InvoiceDeliveryOrder";
	public static final String BLANK_A4 = "Blank_A4_2";


}
